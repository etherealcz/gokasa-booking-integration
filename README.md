# GOKASA Booking Client
This manual will guide you through the integration process of the GOKASA Booking Client

![img](https://res.cloudinary.com/gokasacz/image/upload/v1584542616/booking-demo_xodcdr.png)

## Prerequisites
- This client is a HTML widget and can be easily integrated to any HTML page

## Integration
- Place an `iframe` with the following attributes to you page:
    - `key` - you must obtain this key from the booking service provider, please contact them
    - `locale` - you can specify the language of this widget, supported locales are: 
        - `en` - English (default),
        - `cs` - Czech,
        - `sk` - Slovak,
        - `de` - German,
        - `vi` - Vietnamese.
- Example:
```html
<iframe id="gokasa-booking" src="https://booking.gokasa.cz?key=58bf1cdd6d1193037caf873f&locale=cs"></iframe>
```
- You can resize this widget using inline CSS. The recommended height is `560px`, for example:
```html
<iframe style="border: none; width: 100%; height: 560px" ...></iframe>
```

## Configuration
- Configurations and data settings for this widget (time cliff, services, staff, etc.) are available in the Backend Gokasa POS App - Event Calendar section.

## Demo
- Client - https://booking-demo.gokasa.eu/#rezervace
- Backend - https://bodycare.gokasa.cz
